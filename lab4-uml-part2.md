---
marp: true
theme: gaia
paginate: true
size: 16:9

---
# UML (Unified Modeling Language)：
## 状态图（State Machine Diagram）
## 活动图（Activity Diagram）

---
# UML状态图
* 描述一个对象、子系统或系统的行为。
* 状态图的构成
  * 状态
  * 转移
  * 事件
  * 动作
  * 条件
  ![bg right:60% 100%](figures/lab4-statemachine-diagram.svg)

---
## 状态图 练习
办公室复印机的工作过程如下：未接到复印命令时处于闲置状态，一旦接到复印命令则进入复印状态，完成一个复印命令规定的工作后又回到闲置状态，等待下一个复印命令；如果执行复印命令时发现缺纸，则进入缺纸状态，发出警告，等待装纸，装满纸后进入闲置状态，准备接受复印命令；如果复印时发生卡纸故障，则进入卡纸状态，发出警告，等待维修人员来排除故障，故障排除后回到闲置状态。

请用状态图描绘复印机的行为。

---
# UML活动图
* 活动图表示系统为实现某项目标而发生的流程。
* 活动图的构成
  * 初始节点
  * 活动最终节点
  * 链
  * 动作节点
  * 其它节点（判断、分叉等）
 ![bg right:50% 90%](figures/lab4-activity-diagram-ex1.svg)

---
## 活动（Activity）与动作（Action）
* 活动是被建模的流程
* 动作是流程中所进行的步骤
* 比如：支付是网上购物的一项**活动**，它包含以下**动作**：
  * 选择支付类型
  * 输入帐户信息
  * 系统从账户中扣钱
  * 系统显示结果

---
## 判断节点与合并节点
* 当需要根据不同条件执行不同动作序列时，可以使用判断节点（decision）
* 合并节点（merge）用于表示从判断节点开始的条件式行为到此结束
* Incoming edge 进入链
* Outgoing edge 退出链
* Guard conditions 监护条件，要求：完整性、互斥性
![bg right:50% 100%](figures/lab4-activity-decision.png)

---
## 任务的并行
* 例子：计算机组装工作流
  1. 准备机箱
  2. 准备主板
  3. 安装主板
  4. 安装硬盘和光驱
  5. 安装显卡、声卡和调制解调器
* 其中，动作1和2相互独立，可以同时进行
* Fork 分叉点；Join 结合点
![bg right:50% 80%](figures/lab4-activity-fork.png)

---
## 调用其它活动
![h:400](figures/lab4-activity-diagram-ex2.svg)

---
## 活动分区（Partition）
* 当活动涉及多个参与者时，可以使用活动分区，每个分区显示一个参与者的责任。

![h:400](figures/lab4-activity-partition.svg)