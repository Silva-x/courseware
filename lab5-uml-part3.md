---
marp: true
theme: gaia
paginate: true
size: 16:9

---
# UML (Unified Modeling Language)：
## 顺序图（Sequence Diagram）
## 组件图（Component Diagram）
## 部署图（Deployment Diagram）

---
# UML顺序图
* 顺序图描述系统各个组成部分之间的有序交互。
* 在分析阶段，可用于描述业务流程中各个业务对象之间的交互。
* 在设计阶段，可用于描述程序流程中各个设计对象之间的交互。
* 顺序图的构成
    * 参与者（participant）
    * 生命线（lifeline）
    * 消息（message）

---
## 顺序图的构成
![](figures/lab5-sequence-diagram.jpg)

**活动条（activation bar）**：表示参与者处于活动状态（生命线上的矩形框）

---
## 消息
<style scoped>section { font-size: 30px; }</style>
* 消息的具体形式
    * 函数调用（最常见）
    * 信号的发送
    * 对象的创建和删除
* 消息的类型
    * **同步消息（synchronous message）**：消息发送者等到返回消息之后才能继续工作
    * **异步消息（asynchronous message）**：消息发送者发送消息后，不等待，即继续后面的工作
    * **返回消息（return message）**：表示活动的控制流返回给原始消息的发送者
        * 每一个同步消息都隐含一个后续的返回消息

---
## 顺序图举例
![h:500](figures/lab5-sequence-diagram-ex.svg)

---
# 组件图
* 用于描述子系统（模块）及其之间的关系。
* 组件图的组成
    * 组件（component）
    * 接口（interface）
    * 依赖关系（dependency）

---
## 组件图举例
![h:500](figures/lab5-component-diagram-ex.svg)

---
# 部署图
* 描述软件与其运行环境的映射关系。
* 部署图的构成
    * 工件（artifact）：表示编译、打包后的软件包，比如dll，jar等。
    * 节点（node）：可以运行工件的物理实体。
* 工件 **manifests** 组件
* 工件 is **deployed** on 节点.

---
## 部署图举例
![h:400](figures/lab5-deployment-diagram-ex.svg)
